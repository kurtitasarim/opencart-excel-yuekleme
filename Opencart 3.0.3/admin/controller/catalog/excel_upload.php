<?php

class ControllerCatalogExcelUpload extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/excel');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/category');
//        echo "deneme";
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        // language
        $data['title'] = $this->language->get('text_title');

        $data['action'] = $this->url->link('catalog/excel_upload/upload', 'user_token=' . $this->session->data['user_token'], true);
        // view
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('catalog/excel_upload', $data));
    }

    public function upload()
    {
        // library register
        $this->load->library('excel');
        // excel class ve read
        $excel = Excel::get_instance($this->registry);
        $read = $excel->read($_FILES['excel_file']['tmp_name']);
        echo "<pre>";
        // foreach
        foreach ($read as $key => $value) {
            if ($key > 0) {
                $barkod = $read[$key][0];           //barkod
                $modelKodu = $read[$key][1];           //marka
                $marka = $read[$key][2];           //model kodu
                $kategori = $read[$key][3];           //kategori
                $urunAdi = $read[$key][4];           //ürün adı
                $birim = $read[$key][5];           //birim
                $piyasaFiyati = $read[$key][6];           //piyasa fiyatı
                $kdv = $read[$key][7];           //kdv
                $stok = $read[$key][9];           //stok
                $image = $read[$key][10];          //image
                if($image != null && $image != ""){
                    $rndName = 'catalog/product/' . rand(999999, 99999999999999) . date('dmYhis') . '.jpg';
                    $this->addImage($image, DIR_IMAGE . $rndName);
                    $image = $rndName;
//                    print_r($image);exit;
                }
                // ilk kategori check edelim
                $category = $this->getCategory($kategori);
                $birimOpt = $this->optionAdd($birim, $piyasaFiyati);
                // MArka
                $manufacturer = $this->manufacturer($marka);
                // ürünleri oluşturalım
                $this->getProduct($urunAdi, $modelKodu, $piyasaFiyati, $kdv, $stok, $category[0]['category_id'], $manufacturer[0]['manufacturer_id'], $birimOpt, $image, $birim);
            }
        }
        echo "Excel Yüklendi";
    }

    /**
     * @param $categoryName
     * @return mixed
     * kategori ismini yolluyoruz
     * eğer kategori var ise sana bilgilerini döndürüyor
     * yoksa yeni kategori oluşturup yine bilgileri sana döndürüyor
     */
    public function getCategory($categoryName)
    {
        // model load
        $this->load->model('catalog/category');
        $this->load->model('localisation/language');
        //datalar
        $category['filter_name'] = $categoryName;
        if (count($this->model_catalog_category->getCategories($category)) > 0) {
            return $this->model_catalog_category->getCategories($category);
        } else {
            $newCategory = [];
            foreach ($this->model_localisation_language->getLanguages() as $langKey => $langVal) {
                $newCategory['category_description'][$langVal['language_id']]['name'] = $categoryName;
                $newCategory['category_description'][$langVal['language_id']]['meta_title'] = $categoryName;
            }
            $newCategory['category_store[]'] = 0;
            $this->model_catalog_category->addCategory($newCategory);
            return $this->model_catalog_category->getCategories($category);
        }
    }

    public function getProduct($productName, $productCode, $price, $tax, $stock, $categoryID, $manufacturer = null, $productOpt = [], $image = null, $birim = null)
    {
        // model load
        $this->load->model('catalog/product');
        $this->load->model('localisation/language');
        // dillere göre foreach
        foreach ($this->model_localisation_language->getLanguages() as $langKey => $langVal) {
            $newCategory['product_description'][$langVal['language_id']]['name'] = $productName;
            $newCategory['product_description'][$langVal['language_id']]['meta_title'] = $productName;
            $newCategory['product_description'][$langVal['language_id']]['tag'] = $productName;
        }
        $newCategory['model'] = $productCode;
        $newCategory['price'] = ($birim == "KG") ? NULL : $price;
        $newCategory['quantity'] = $stock;
        $newCategory['minimum'] = 1;
        $newCategory['manufacturer_id'] = $manufacturer;
        $newCategory['product_category'][] = $categoryID;
        $newCategory['product_store'][] = 0;
        $newCategory['product_option'] = $productOpt;
        $newCategory['image'] = $image;
//        echo "<pre>"; print_r($newCategory);exit;
        $this->model_catalog_product->addProduct($newCategory);
    }

    public function manufacturer($manufacturer)
    {
        $this->load->model('catalog/manufacturer');
        $filter_data = array(
            'filter_name' => $manufacturer,
            'start' => 0,
            'limit' => 1
        );
        if (count($this->model_catalog_manufacturer->getManufacturers($filter_data)) == 0) {
            $uretici['name'] = $manufacturer;
            $uretici['manufacturer_store'][] = 0;
            $this->model_catalog_manufacturer->addManufacturer($uretici);
        }
        return $this->model_catalog_manufacturer->getManufacturers($filter_data);
    }

    public function optionAdd($birim, $fiyat)
    {
        $this->load->model('catalog/option');
        $filter = [
            'filter_name' => 'Gram',
        ];
        $option = $this->model_catalog_option->getOptions($filter);
        if ($option == null) {
            echo "Gram özelliği tanımlı olmadığı için devam edilmemektedir!";
            exit;
        }
        if ($birim == "KG") {
            $return = [];
            foreach ($this->model_catalog_option->getOptionValues($option[0]['option_id']) as $key => $val) {
                $return['product_option']['product_option_value'][$key]['option_value_id'] = $val['option_value_id'];
                $return['product_option']['product_option_value'][$key]['quantity'] = 1;
                $return['product_option']['product_option_value'][$key]['subtract'] = 1;
                $return['product_option']['product_option_value'][$key]['price_prefix'] = "+";
                if ($val['name'] > 10) {
                    $carpan = $fiyat * ($val['name'] / 1000);
                } else {
                    $carpan = $fiyat * $val['name'];
                }
                $return['product_option']['product_option_value'][$key]['price'] = $carpan;
                $return['product_option']['product_option_value'][$key]['points_prefix'] = "+";
                $return['product_option']['product_option_value'][$key]['points'] = null;
                $return['product_option']['product_option_value'][$key]['weight_prefix'] = "+";
                $return['product_option']['product_option_value'][$key]['weight'] = $val['name'];
            }
            $return['product_option']['product_option_id'] = null;
            $return['product_option']['name'] = $option[0]['name'];
            $return['product_option']['option_id'] = $option[0]['option_id'];
            $return['product_option']['type'] = $option[0]['type'];
            $return['product_option']['required'] = 1;
            return $return;
        }
    }

    public function addImage($image, $saveTo)
    {
        $this->load->model('tool/image');
        $ch = curl_init($image);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveTo)) {
            unlink($saveTo);
        }
        $fp = fopen($saveTo, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }

}