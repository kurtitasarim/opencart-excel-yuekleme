<?php

use shuchkin\SimpleXLSX;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/shuchkin/SimpleXLSX.php';

Class Excel
{
    private static $instance;
    public static function get_instance($registry) {
        if (is_null(static::$instance)) {
            static::$instance = new static($registry);
        }

        return static::$instance;
    }
    public function read($file)
    {
        if ($xlsx = SimpleXLSX::parse($file)) {
            return $xlsx->rows();
        } else {
            echo SimpleXLSX::parseError();
        }
    }
}